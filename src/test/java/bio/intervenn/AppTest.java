package bio.intervenn;

import org.eurocarbdb.application.glycanbuilder.BuilderWorkspace;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FileUtils;
import org.eurocarbdb.application.glycanbuilder.Glycan;
import org.eurocarbdb.application.glycanbuilder.ResidueType;
import org.eurocarbdb.application.glycanbuilder.massutil.MassOptions;
import org.eurocarbdb.application.glycanbuilder.renderutil.GlycanRendererAWT;
import org.eurocarbdb.application.glycanbuilder.util.GraphicOptions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import javax.imageio.ImageIO;

/**
 * Unit test for simple App.
 */
class AppTest {
    /**
     * Rigorous Test.
     */
    @Test
    void testApp() {
        assertEquals(1, 1);
    }

    @Test
    public void testCT() throws Exception {

        MassOptions massOptions = new MassOptions();
        massOptions.setDerivatization(MassOptions.NO_DERIVATIZATION);
        String redEnd = "freeEnd";
        massOptions.setReducingEndTypeString(redEnd);
        //massOptions.isUndetermined();


        String  glycoCT = "RES\n" +
                "1b:x-dglc-HEX-1:5\n" +
                "2b:b-dgal-HEX-1:5\n" +
                "3b:b-dglc-HEX-1:5\n" +
                "4s:n-acetyl\n" +
                "5b:b-dgal-HEX-1:5\n" +
                "6b:b-dglc-HEX-1:5\n" +
                "7s:n-acetyl\n" +
                "8b:b-dgal-HEX-1:5\n" +
                "LIN\n" +
                "1:1o(4+1)2d\n" +
                "2:2o(3+1)3d\n" +
                "3:3d(2+1)4n\n" +
                "4:3o(4+1)5d\n" +
                "5:5o(3+1)6d\n" +
                "6:6d(2+1)7n\n" +
                "7:6o(4+1)8d";

        BuilderWorkspace work = new BuilderWorkspace(new GlycanRendererAWT());
        work.initData();
        work.setNotation(GraphicOptions.NOTATION_SNFG);

        ResidueType residueType = ResidueType.createOtherReducingEnd("proc", -1);
        massOptions.setReducingEndType(residueType);


        Glycan glycan = Glycan.fromGlycoCTCondensed(glycoCT, massOptions);
        BufferedImage img = work.getGlycanRenderer().getImage(glycan, true, false, true, 10);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        File outputfile = new File("/tmp/lg-snfg-GS30180.png");
        ImageIO.write(img, "png", byteArrayOutputStream);
        ImageIO.write(img, "png", outputfile);

        BufferedImage img2 = work.getGlycanRenderer().getImage(glycan, true, false, true, 1);

        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        File outputfile2 = new File("/tmp/sm-snfg-GS30180.png");
        ImageIO.write(img2, "png", byteArrayOutputStream2);
        ImageIO.write(img2, "png", outputfile2);

        assertTrue(outputfile.exists());

    }

    @Test
    public void testComp() throws Exception {

        String hexComp = "--??1Hex,p)";
        String hexnacComp = "--??1HexNAc,p)";
        String dhexComp = "--??1dHex,p)";
        String neugcComp = "--??2D-NeuGc,p)";
        String neuacComp = "--??2D-NeuAc,p)";
        String xylComp = "--??1Pen)";

        File file = new File("/tmp/comp.txt");
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        try {
            while (it.hasNext()) {

                int hex = 0;
                int hexnac = 0;
                int dhex = 0;
                int neugc = 0;
                int neuac = 0;
                int xyl = 0;
                String gws = "";
                String line = it.nextLine();
                // System.out.println("comp: " + line);
                String regex = "(?<=\\d)(?=\\D)";
                String[] residues = line.split(regex);
                for(String residue : residues) {
                    //System.out.println("check " + residue);
                    if (residue.toLowerCase().trim().matches("hexa\\d+") || residue.toLowerCase().trim().matches("kdn\\d+") || residue.toLowerCase().trim().matches("pent\\d+") || residue.toLowerCase().trim().matches("p\\d+") || residue.toLowerCase().trim().matches("s\\d+")) {
                        continue;
                    }
                    if (residue.toLowerCase().trim().startsWith("hexnac")) {
                        hexnac = Integer.parseInt(residue.toLowerCase().trim().replaceAll("hexnac", ""));
                        gws += StringUtils.repeat(hexnacComp, hexnac);

                    } else if (residue.toLowerCase().trim().startsWith("hex")) {
                        hex = Integer.parseInt(residue.toLowerCase().trim().replaceAll("hex", ""));
                        gws += StringUtils.repeat(hexComp, hex);

                    } else if (residue.toLowerCase().trim().startsWith("dhex")) {
                        dhex = Integer.parseInt(residue.toLowerCase().trim().replaceAll("dhex", ""));
                        gws += StringUtils.repeat(dhexComp, dhex);

                    } else if (residue.toLowerCase().trim().startsWith("neugc")) {
                        neugc = Integer.parseInt(residue.toLowerCase().trim().replaceAll("neugc", ""));
                        gws += StringUtils.repeat(neugcComp, neugc);

                    } else if (residue.toLowerCase().trim().startsWith("neuac")) {
                        neuac = Integer.parseInt(residue.toLowerCase().trim().replaceAll("neuac", ""));
                        gws += StringUtils.repeat(neuacComp, neuac);

                    } else if (residue.toLowerCase().trim().startsWith("xyl")) {
                        xyl = Integer.parseInt(residue.toLowerCase().trim().replaceAll("xyl", ""));
                        gws += StringUtils.repeat(xylComp, xyl);

                    }

                }
                System.out.println("gws: " + gws);
                int count = StringUtils.countMatches(gws, ")");
                String startGws = "freeEnd}" + StringUtils.repeat("(", count-1);
                String gwsChop = StringUtils.chop(gws);
                String finalGws = startGws + gwsChop + "$MONO,perMe,Na,0,freeEnd";
                System.out.println("count: " + count);
                System.out.println("final: " + finalGws);

                //MassOptions massOptions = new MassOptions();
                BuilderWorkspace work = new BuilderWorkspace(new GlycanRendererAWT() );
                work.initData();
                work.setNotation(GraphicOptions.NOTATION_SNFG);
                //Glycan glycan = Glycan.fromString(finalGws, "")
                Glycan glycan = Glycan.fromString(finalGws);
                BufferedImage img = work.getGlycanRenderer().getImage(glycan, true, false, true, 5);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                File outputfile = new File("/tmp/comp_" + line.trim() + ".png");
                ImageIO.write(img, "png", byteArrayOutputStream);
                ImageIO.write(img, "png", outputfile);

                assertTrue(outputfile.exists());

            }
        } finally {
            LineIterator.closeQuietly(it);
        }


    }
}
